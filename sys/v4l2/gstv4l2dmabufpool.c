/* GStreamer
 *
 * Copyright (C) 2001-2002 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *               2006 Edgard Lima <edgard.lima@indt.org.br>
 *               2009 Texas Instruments, Inc - http://www.ti.com/
 *
 * gstv4l2bufferpool.c V4L2 buffer pool class
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/mman.h>
#include <string.h>
#include <unistd.h>

#include "gst/video/video.h"
#include <gstv4l2dmabufpool.h>
#include <gst/dmabuf/dmabuf.h>

#include "gstv4l2src.h"
#ifdef HAVE_EXPERIMENTAL
#include "gstv4l2sink.h"
#endif
#include "v4l2_calls.h"
#include "gst/gst-i18n-plugin.h"
#include <gst/glib-compat-private.h>

/* videodev2.h is not versioned and we can't easily check for the presence
 * of enum values at compile time, but the V4L2_CAP_VIDEO_OUTPUT_OVERLAY define
 * was added in the same commit as V4L2_FIELD_INTERLACED_{TB,BT} (b2787845) */
#ifndef V4L2_CAP_VIDEO_OUTPUT_OVERLAY
#define V4L2_FIELD_INTERLACED_TB 8
#define V4L2_FIELD_INTERLACED_BT 9
#endif


GST_DEBUG_CATEGORY_EXTERN (v4l2_debug);
#define GST_CAT_DEFAULT v4l2_debug

static GstMiniObjectClass *dmabuf_pool_parent_class = NULL;

static void
gst_v4l2_dmabuf_pool_finalize (GstV4l2DmabufPool * pool)
{
  int n;
  g_mutex_free (pool->lock);
  pool->lock = NULL;

  if (pool->video_fd >= 0)
    v4l2_close (pool->video_fd);

  for (n = 0; n < pool->buffer_count; n++) {
    if (pool->buffers[n].buffer && pool->buffers[n].queued_flag == TRUE) {
      /* These are the buffers we are holding, free them */
      gst_buffer_unref (pool->buffers[n].buffer);
    }
  }

  if (pool->buffers) {
    g_free (pool->buffers);
    pool->buffers = NULL;
  }

  if (pool->htable) {
    g_hash_table_unref (pool->htable);
    pool->htable = NULL;
  }

  if (pool->caps) {
    gst_caps_unref (pool->caps);
    pool->caps = NULL;
  }

  if (pool->thr) {
    g_thread_unref (pool->thr);
    pool->thr = NULL;
  }

  GST_MINI_OBJECT_CLASS (dmabuf_pool_parent_class)->finalize (GST_MINI_OBJECT
      (pool));
}

static void
gst_v4l2_dmabuf_pool_init (GstV4l2BufferPool * pool, gpointer g_class)
{
  pool->lock = g_mutex_new ();
  pool->running = FALSE;
}

static void
gst_v4l2_dmabuf_pool_class_init (gpointer g_class, gpointer class_data)
{
  GstMiniObjectClass *mini_object_class = GST_MINI_OBJECT_CLASS (g_class);

  dmabuf_pool_parent_class = g_type_class_peek_parent (g_class);

  mini_object_class->finalize = (GstMiniObjectFinalizeFunction)
      gst_v4l2_dmabuf_pool_finalize;
}

GType
gst_v4l2_dmabuf_pool_get_type (void)
{
  static GType _gst_v4l2_dmabuf_pool_type;

  if (G_UNLIKELY (_gst_v4l2_dmabuf_pool_type == 0)) {
    static const GTypeInfo v4l2_dmabuf_pool_info = {
      sizeof (GstMiniObjectClass),
      NULL,
      NULL,
      gst_v4l2_dmabuf_pool_class_init,
      NULL,
      NULL,
      sizeof (GstV4l2BufferPool),
      0,
      (GInstanceInitFunc) gst_v4l2_dmabuf_pool_init,
      NULL
    };
    _gst_v4l2_dmabuf_pool_type = g_type_register_static (GST_TYPE_MINI_OBJECT,
        "GstV4l2DmabufPool", &v4l2_dmabuf_pool_info, 0);
  }
  return _gst_v4l2_dmabuf_pool_type;
}


/**
 * gst_v4l2_dmabuf_pool_thr: thread function running to pend of gst_pad_alloc_buffer
 * and queue it into the driver
 */
static gpointer
gst_v4l2_dmabuf_pool_thr (gpointer data)
{
  GstV4l2DmabufPool *pool =
      (GstV4l2DmabufPool *) gst_mini_object_ref (GST_MINI_OBJECT (data));
  GstBuffer *outbuf;
  GstFlowReturn ret;
  gboolean flag;
  int index;

  while (1) {
    GST_V4L2_DMABUF_POOL_LOCK (pool);
    flag = pool->running;
    GST_V4L2_DMABUF_POOL_UNLOCK (pool);
    if (!flag)
      break;

    ret = gst_pad_alloc_buffer (pool->pad, 0, 0,        // Let the peer element determine buffersize from caps
        pool->caps, &outbuf);
    if (ret != GST_FLOW_OK) {
      GST_CAT_ERROR (v4l2_debug, "alloc_buffer failed %s",
          gst_flow_get_name (ret));
      break;
    }
    /* Lookup the buffer in our hash table */
    index = (int) g_hash_table_lookup (pool->htable, outbuf);
    if (0 == index) {
      GST_CAT_ERROR (v4l2_debug, "allocated buffer is not found in hash table");
      break;
    }
    index--;
    /* Check if the buffer is not actually queued earlier */
    GST_V4L2_DMABUF_POOL_LOCK (pool);
    if (pool->running) {
      flag = pool->buffers[index].queued_flag;
      if (flag) {
        GST_V4L2_DMABUF_POOL_UNLOCK (pool);
        GST_CAT_ERROR (v4l2_debug, "Unexpected! buffer was already queued");
        break;
      }

      if (v4l2_ioctl (pool->video_fd, VIDIOC_QBUF,
              &pool->buffers[index].vbuffer) < 0) {
        GST_V4L2_DMABUF_POOL_UNLOCK (pool);
        GST_CAT_ERROR (v4l2_debug, "QBUF failed");
        break;
      }
      pool->buffers[index].queued_flag = TRUE;
      GST_V4L2_DMABUF_POOL_UNLOCK (pool);
    } else {
      GST_V4L2_DMABUF_POOL_UNLOCK (pool);
      gst_buffer_unref (outbuf);
    }
  }
  gst_mini_object_unref (GST_MINI_OBJECT (pool));
  return NULL;
}

/**
 * gst_v4l2_dmabuf_pool_new:
 * @fd:   the video device file descriptor
 * @num_buffers:  the requested number of buffers in the pool
 * @caps:  the caps to set on the buffer
 *
 * Construct a new buffer pool.
 *
 * Returns: the new pool, use gst_v4l2_dmabuf_pool_destroy() to free resources
 */
GstV4l2DmabufPool *
gst_v4l2_dmabuf_pool_new (gint fd, gint num_buffers, GstCaps * caps,
    GstPad * pad, enum v4l2_buf_type type)
{
  GstV4l2DmabufPool *pool;
  gint n;
  struct v4l2_requestbuffers breq;
  GstBuffer *outbuf;
  GstFlowReturn ret;

  pool = (GstV4l2DmabufPool *) gst_mini_object_new (GST_TYPE_V4L2_DMABUF_POOL);

  pool->video_fd = v4l2_dup (fd);
  if (pool->video_fd < 0)
    goto dup_failed;


  /* first, lets request buffers, and see how many we can get: */
  GST_CAT_DEBUG (v4l2_debug, "STREAMING, requesting %d DMABUF buffers",
      num_buffers);

  memset (&breq, 0, sizeof (struct v4l2_requestbuffers));
  breq.type = type;
  breq.count = num_buffers;
  breq.memory = V4L2_MEMORY_DMABUF;

  if (v4l2_ioctl (fd, VIDIOC_REQBUFS, &breq) < 0)
    goto reqbufs_failed;

  GST_CAT_LOG (v4l2_debug, " count:  %u", breq.count);
  GST_CAT_LOG (v4l2_debug, " type:   %d", breq.type);
  GST_CAT_LOG (v4l2_debug, " memory: %d", breq.memory);

  if (breq.count < GST_V4L2_MIN_BUFFERS)
    goto no_buffers;

  if (num_buffers != breq.count) {
    GST_CAT_WARNING (v4l2_debug, "using %u buffers instead", breq.count);
    num_buffers = breq.count;
  }

  pool->type = type;
  pool->buffer_count = num_buffers;
  pool->buffers = g_malloc0 (sizeof (GstV4l2Buffer) * num_buffers);
  pool->htable =
      g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL, NULL);
  pool->pad = pad;
  pool->caps = gst_caps_copy (caps);

  /* now, allocate the buffers from peer element: */
  for (n = 0; n < num_buffers; n++) {
    ret = gst_pad_alloc_buffer (pad, 0, 0,      // Let the peer element determine buffersize from caps
        caps, &outbuf);
    if (ret != GST_FLOW_OK) {
      GST_CAT_ERROR (v4l2_debug, "alloc_buffer failed %s",
          gst_flow_get_name (ret));
      goto buffer_new_failed;
    }
    pool->buffers[n].buffer = outbuf;
  }

  /* call VIDIOC_QUERYBUF */
  for (n = 0; n < num_buffers; n++) {
    GstDmaBuf *dmabuf = gst_buffer_get_dma_buf (pool->buffers[n].buffer);
    if (!dmabuf) {
      GST_CAT_ERROR (v4l2_debug, "gst_buffer_get_dma_buf failed");
      goto buffer_new_failed;
    }
    pool->buffers[n].vbuffer = (struct v4l2_buffer) {
    .type = type,.memory = V4L2_MEMORY_DMABUF,.index = n,.m.fd =
          gst_dma_buf_get_fd (dmabuf),};
    if (v4l2_ioctl (fd, VIDIOC_QUERYBUF, &pool->buffers[n].vbuffer) < 0) {
      GST_CAT_ERROR (v4l2_debug, "QUERYBUF failed");
      goto buffer_new_failed;
    }
    pool->buffers[n].vbuffer.m.fd = gst_dma_buf_get_fd (dmabuf);

    /* Add an a hash table entry, n+1 to avoid NULL */
    g_hash_table_insert (pool->htable, pool->buffers[n].buffer,
        (gpointer) (n + 1));
  }

  /* call VIDIOC_QBUF */
  for (n = 0; n < num_buffers; n++) {
    if (v4l2_ioctl (fd, VIDIOC_QBUF, &pool->buffers[n].vbuffer) < 0) {
      GST_CAT_ERROR (v4l2_debug, "Initial QBUF failed");
      goto buffer_new_failed;
    }
    pool->buffers[n].queued_flag = TRUE;
  }

  return pool;

  /* ERRORS */
dup_failed:
  {
    gint errnosave = errno;
    gst_v4l2_dmabuf_pool_destroy (pool);
    errno = errnosave;
    return NULL;
  }
reqbufs_failed:
  {
    GST_CAT_ERROR (v4l2_debug,
        "error requesting %d buffers: %s", num_buffers, g_strerror (errno));
    gst_v4l2_dmabuf_pool_destroy (pool);
    return NULL;
  }
no_buffers:
  {
    GST_CAT_ERROR (v4l2_debug,
        "we received %d, we want at least %d", breq.count,
        GST_V4L2_MIN_BUFFERS);
    gst_v4l2_dmabuf_pool_destroy (pool);
    return NULL;
  }
buffer_new_failed:
  {
    gint errnosave = errno;
    gst_v4l2_dmabuf_pool_destroy (pool);
    errno = errnosave;
    return NULL;
  }
}

/**
 * gst_v4l2_dmabuf_pool_start:
 * @pool: the pool
 *
 * Called before start streaming... starts the QBUF thread
 */
gboolean
gst_v4l2_dmabuf_pool_start (GstV4l2DmabufPool * pool)
{
  GST_V4L2_DMABUF_POOL_LOCK (pool);
  if (pool->running == FALSE) {
    /* Create a thread to pend of bufferalloc and qbuffers immediately */
    pool->thr = g_thread_new ("v4l2dmabufpool", gst_v4l2_dmabuf_pool_thr, pool);
    if (!pool->thr) {
      GST_V4L2_DMABUF_POOL_UNLOCK (pool);
      GST_CAT_ERROR (v4l2_debug, "g_thread_new failed");
      return FALSE;
    }
    pool->running = TRUE;
  }
  GST_V4L2_DMABUF_POOL_UNLOCK (pool);
  return TRUE;
}

/**
 * gst_v4l2_dmabuf_pool_stop:
 * @pool: the pool
 *
 * Stops the QBUF thread
 */
void
gst_v4l2_dmabuf_pool_stop (GstV4l2DmabufPool * pool)
{
  GST_V4L2_DMABUF_POOL_LOCK (pool);
  pool->running = FALSE;
  GST_V4L2_DMABUF_POOL_UNLOCK (pool);
}

/**
 * gst_v4l2_dmabuf_pool_destroy:
 * @pool: the pool
 *
 * Free all resources in the pool and the pool itself.
 */
void
gst_v4l2_dmabuf_pool_destroy (GstV4l2DmabufPool * pool)
{
  GST_V4L2_DMABUF_POOL_LOCK (pool);
  pool->running = FALSE;
  GST_V4L2_DMABUF_POOL_UNLOCK (pool);

  GST_CAT_DEBUG (v4l2_debug, "destroy pool");

  gst_mini_object_unref (GST_MINI_OBJECT (pool));
}


/**
 * gst_v4l2_dmabuf_pool_dqbuf:
 * @pool: the pool
 *
 * Dequeue a buffer from the driver.  Some generic error handling is done in
 * this function, but any error handling specific to v4l2src (capture) or
 * v4l2sink (output) can be done outside this function by checking 'errno'
 *
 * Returns: a buffer
 */
GstBuffer *
gst_v4l2_dmabuf_pool_dqbuf (GstV4l2DmabufPool * pool)
{
  GstBuffer *pool_buffer;
  struct v4l2_buffer buffer;

  memset (&buffer, 0x00, sizeof (buffer));
  buffer.type = pool->type;
  buffer.memory = V4L2_MEMORY_DMABUF;


  if (v4l2_ioctl (pool->video_fd, VIDIOC_DQBUF, &buffer) >= 0) {

    GST_V4L2_DMABUF_POOL_LOCK (pool);

    /* get our GstBuffer with that index from the pool, if the buffer was
     * outstanding we have a serious problem.
     */
    pool_buffer = pool->buffers[buffer.index].buffer;

    if (pool_buffer == NULL) {
      GST_V4L2_DMABUF_POOL_UNLOCK (pool);
      GST_CAT_ERROR (v4l2_debug,
          "No free buffers found in the pool at index %d.", buffer.index);
      return NULL;
    }

    GST_CAT_LOG (v4l2_debug,
        "grabbed frame %d (ix=%d), flags %08x, buffer=%p",
        buffer.sequence, buffer.index, buffer.flags, pool_buffer);

    /* set top/bottom field first if v4l2_buffer has the information */
    if (buffer.field == V4L2_FIELD_INTERLACED_TB)
      GST_BUFFER_FLAG_SET (pool_buffer, GST_VIDEO_BUFFER_TFF);
    if (buffer.field == V4L2_FIELD_INTERLACED_BT)
      GST_BUFFER_FLAG_UNSET (pool_buffer, GST_VIDEO_BUFFER_TFF);

    /* this can change at every frame, esp. with jpeg */
    GST_BUFFER_SIZE (pool_buffer) = buffer.bytesused;

    pool->buffers[buffer.index].queued_flag = FALSE;

    GST_V4L2_DMABUF_POOL_UNLOCK (pool);

    return pool_buffer;
  }


  GST_CAT_WARNING (v4l2_debug,
      "problem grabbing frame %d (ix=%d), pool-ct=%d, buf.flags=%d",
      buffer.sequence, buffer.index,
      GST_MINI_OBJECT_REFCOUNT (pool), buffer.flags);

  switch (errno) {
    case EAGAIN:
      GST_CAT_WARNING (v4l2_debug,
          "Non-blocking I/O has been selected using O_NONBLOCK and"
          " no buffer was in the outgoing queue.");
      break;
    case EINVAL:
      GST_CAT_ERROR (v4l2_debug,
          "The buffer type is not supported, or the index is out of bounds,"
          " or no buffers have been allocated yet, or the userptr"
          " or length are invalid.");
      break;
    case ENOMEM:
      GST_CAT_ERROR (v4l2_debug,
          "insufficient memory to enqueue a user pointer buffer.");
      break;
    case EIO:
      GST_CAT_INFO (v4l2_debug,
          "VIDIOC_DQBUF failed due to an internal error."
          " Can also indicate temporary problems like signal loss."
          " Note the driver might dequeue an (empty) buffer despite"
          " returning an error, or even stop capturing.");
      /* have we de-queued a buffer ? */
      if (!(buffer.flags & (V4L2_BUF_FLAG_QUEUED | V4L2_BUF_FLAG_DONE))) {
        GST_CAT_DEBUG (v4l2_debug, "reenqueing buffer");
        /* FIXME ... should we do something here? */
      }
      break;
    case EINTR:
      GST_CAT_WARNING (v4l2_debug, "could not sync on a buffer");
      break;
    default:
      GST_CAT_WARNING (v4l2_debug,
          "Grabbing frame got interrupted unexpectedly. %d: %s.",
          errno, g_strerror (errno));
      break;
  }

  return NULL;
}
