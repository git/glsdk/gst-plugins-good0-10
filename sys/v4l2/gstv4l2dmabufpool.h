/* GStreamer
 *
 * Copyright (C) 2001-2002 Ronald Bultje <rbultje@ronald.bitfreak.net>
 *               2006 Edgard Lima <edgard.lima@indt.org.br>
 *               2009 Texas Instruments, Inc - http://www.ti.com/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GSTV4L2_DMABUF_POOL_H__
#define __GSTV4L2_DMABUF_POOL_H__

#include <gst/gst.h>
#include "v4l2_calls.h"

typedef struct _GstV4l2DmabufPool GstV4l2DmabufPool;

GType gst_v4l2_dmabuf_pool_get_type (void);
#define GST_TYPE_V4L2_DMABUF_POOL (gst_v4l2_dmabuf_pool_get_type())
#define GST_IS_V4L2_DMABUF_POOL(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_V4L2_DMABUF_POOL))
#define GST_V4L2_DMABUF_POOL(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_V4L2_DMABUF_POOL, GstV4l2DmabufPool))

typedef struct _GstV4l2Dmabuf GstV4l2Dmabuf;

struct _GstV4l2DmabufPool
{
  GstMiniObject base;

  enum v4l2_buf_type type;      /* V4L2_BUF_TYPE_VIDEO_CAPTURE, V4L2_BUF_TYPE_VIDEO_OUTPUT */
  GstPad *pad;                  /* Pad for buffer allocation from peer element */
  GstCaps *caps;

  GMutex *lock;
  gboolean running;             /* with lock */
  GThread *thr;                 /* Thread to try and alloc buffers from peer element */
  gint video_fd;                /* a dup(2) of the v4l2object's video_fd */
  guint buffer_count;
  GstV4l2Dmabuf *buffers;       /* Array to store buffer pointers */
  GHashTable *htable;           /* Hash table to map GstBuffer pointer to 'buffers' array index pointers */
};

struct _GstV4l2Dmabuf
{
  GstBuffer *buffer;
  struct v4l2_buffer vbuffer;
  gboolean queued_flag;         /* Flag tells if this buffer is queued into the driver */
};

void gst_v4l2_dmabuf_pool_destroy (GstV4l2DmabufPool * pool);

GstV4l2DmabufPool *gst_v4l2_dmabuf_pool_new (gint fd, gint num_buffers,
    GstCaps * caps, GstPad * pad, enum v4l2_buf_type type);

gboolean gst_v4l2_dmabuf_pool_start (GstV4l2DmabufPool * pool);
void gst_v4l2_dmabuf_pool_stop (GstV4l2DmabufPool * pool);

GstBuffer *gst_v4l2_dmabuf_pool_dqbuf (GstV4l2DmabufPool * pool);

#define GST_V4L2_DMABUF_POOL_LOCK(pool)     g_mutex_lock ((pool)->lock)
#define GST_V4L2_DMABUF_POOL_UNLOCK(pool)   g_mutex_unlock ((pool)->lock)

G_END_DECLS
#endif /* __GSTV4L2_DMABUF_POOL_H__ */
